import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:downloads_path_provider/downloads_path_provider.dart';
import '../models/log.dart';
import '../blocs/log_cubit.dart';

class LogPage extends StatelessWidget {
  final _logCubit = LogCubit();

  LogPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text("Workout Time!"), actions: [
        IconButton(
            icon: const Icon(Icons.file_download),
            onPressed: () async {
              Directory downloadsDir =
                  await DownloadsPathProvider.downloadsDirectory;
              File('${downloadsDir.path}/WorkoutTime Log.json')
                  .writeAsString(jsonEncode(_logCubit.state))
                  .then((_) => ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                          content:
                              Text(AppLocalizations.of(context).downloadLog))));
              _logCubit.close();
            })
      ]),
      body: BlocBuilder<LogCubit, List<Log>>(
          bloc: _logCubit,
          builder: (context, log) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: ListView.builder(
                  itemCount: log.length,
                  itemBuilder: (BuildContext context, int index) => TimelineTile(
                      alignment: TimelineAlign.manual,
                      lineXY: 0.2,
                      isFirst: index == 0,
                      isLast: index == log.length - 1,
                      startChild: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child:
                              Text(DateFormat('MMM d\nH:mm').format(DateTime.fromMillisecondsSinceEpoch(log[index].timestamp)),
                                  style: const TextStyle(fontSize: 14))),
                      endChild: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: Text(log[index].title,
                              style: const TextStyle(fontSize: 16))))))));
}
