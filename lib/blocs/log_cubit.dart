import 'package:hydrated_bloc/hydrated_bloc.dart';
import '../models/log.dart';

class LogCubit extends HydratedCubit<List<Log>> {
  LogCubit() : super([]);

  addLog(String title) =>
      emit([Log(DateTime.now().millisecondsSinceEpoch, title), ...state]);

  @override
  List<Log> fromJson(Map<String, dynamic> json) {
    List<Log> log = [];
    json['log'].forEach((el) => log.add(Log.fromJson(el)));
    return log;
  }

  @override
  Map<String, dynamic> toJson(List<Log> state) {
    if (state is List<Log>) {
      var json = {'log': []};
      for (var log in state) {
        json['log'].add(log.toJson());
      }
      return json;
    } else {
      return null;
    }
  }
}
