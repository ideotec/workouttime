import 'package:equatable/equatable.dart';

class Settings extends Equatable {
  final String tickSound;
  final String exEndSound;
  final String woEndSound;

  const Settings(this.tickSound, this.exEndSound, this.woEndSound);

  factory Settings.fromJson(Map<String, dynamic> json) => Settings(
      json['tickSound'] as String,
      json['exEndSound'] as String,
      json['woEndSound'] as String);

  Map<String, dynamic> toJson() => {
        'tickSound': tickSound,
        'exEndSound': exEndSound,
        'woEndSound': woEndSound
      };

  @override
  List<Object> get props => [tickSound, exEndSound, woEndSound];
}
